require 'swagger_helper'

describe 'AntonymPersonValueGroup API' do
  path '/antonym_person_value_groups/{id}' do

    get 'Retrieves a antonym person value group' do
      tags 'AntonymPersonValueGroups'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Antonym person value group found' do
        schema '$ref' => '#/definitions/antonym_person_value_group_map' 
	run_test!
      end
      response '404', 'Antonym person value group not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

    put 'Updates a antonym person value group' do
      tags 'AntonymPersonValueGroups'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :antonym_person_value_group, in: :body, 
        schema: {"$ref" => '#/definitions/send_antonym_person_value_group_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'The antonym person value group update' do
        schema '$ref' => '#/definitions/antonym_person_value_group_map' 
	run_test!
      end
      response '404', 'Antonym person value group not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 

    delete 'Deletes a antonym person value group' do
      tags 'AntonymPersonValueGroups'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'Success' do
	run_test!
      end
      response '404', 'Antonym not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end 
  end
  path '/antonym_person_value_groups' do
    post 'Create a antonym person value group' do
      tags 'AntonymPersonValueGroups'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :antonym_person_value_group, in: :body, 
        schema: {"$ref" => '#/definitions/send_antonym_person_value_group_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Created antonym person value group' do
        schema '$ref' => '#/definitions/antonym_person_value_group_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 
    get 'Retrieves all antonym person value group' do
      tags 'AntonymPersonValueGroups'
      produces 'application/json'
      parameter name: "filterrific[with_personal_value_group_id]", :in => 'query', :type => :integer,
                required: false, :description => "Personal value group id"
      response '200', 'Antonym person value group found' do
        schema '$ref' => '#/definitions/antonym_person_value_groups_map'
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

  end
end
