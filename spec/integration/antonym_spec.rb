require 'swagger_helper'

describe 'Antonym API' do
  path '/antonyms/{id}' do

    get 'Retrieves a antonym' do
      tags 'Antonyms'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Antonym found' do
        schema '$ref' => '#/definitions/antonym_map' 
	run_test!
      end
      response '404', 'Antonym not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

    put 'Updates a antonym' do
      tags 'Antonyms'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :antonym, in: :body, 
        schema: {"$ref" => '#/definitions/send_antonym_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'The antonym update' do
        schema '$ref' => '#/definitions/antonym_map' 
	run_test!
      end
      response '404', 'Antonym not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 

    delete 'Deletes a antonym' do
      tags 'Antonyms'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'Success' do
	run_test!
      end
      response '404', 'Antonym not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end 
  end
  path '/antonyms' do
    post 'Create a antonym' do
      tags 'Antonyms'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :antonym, in: :body, 
        schema: {"$ref" => '#/definitions/send_antonym_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Created antonym' do
        schema '$ref' => '#/definitions/antonym_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 
    get 'Retrieves all antonym' do
      tags 'Antonyms'
      produces 'application/json'
      parameter name: "filterrific[with_characteristic_id]", :in => 'query', :type => :integer,
                required: false, :description => "Characteristic id"
      response '200', 'Antonym found' do
        schema '$ref' => '#/definitions/antonyms_map'
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

  end
end
