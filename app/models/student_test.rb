class StudentTest < ApplicationRecord
  belongs_to :student
  belongs_to :test
  belongs_to :student_test_result, optional: true, dependent: :destroy
  has_many :persons, dependent: :destroy

  validates_uniqueness_of :test_id, scope: :student_id

  accepts_nested_attributes_for :persons, allow_destroy: true

  filterrific available_filters: %i[
      with_student_id
      with_test_id
  ]

  scope :with_student_id, lambda { |id|
    where(student_id: [*id])
  }

  scope :with_test_id, lambda { |id|
    where(test_id: [*id])
  }

  def roles_hash
    res = {}
    persons.each do |p|
      p.person_roles.each do |pr|
        key = pr.role.name
        if res.key?(key)
          res[key] += 1
        else
          res[key] = 1
        end
      end
    end
    res
  end

  def groups_hash
    res = {}
    persons.each do |p|
      p.person_characteristics.each do |pc|
        key = pc.characteristic.personal_value_group.name
        if res.key?(key)
          res[key] += 1
        else
          res[key] = 1
        end
      end
    end
    res
  end

  def hist_role_personal_value_group_count
    res = {}
    persons.each do |p|
      p.person_characteristics.each do |pc|
        group = pc.characteristic.personal_value_group
        p.person_roles.each do |pr|
          role = pr.role
          if res.key?([role.id, group.id])
            res[[role.id, group.id]] += 1
          else
            res[[role.id, group.id]] = 1
          end
        end
      end
    end
    res
  end

  def ideals
    threshold = [me.y, 0.75].max
    like_me_persons = like_me
    ups = persons.to_a.select { |item| item.y > threshold && !like_me_persons.include?(item) }
    ups.empty? ? [] : slice_persons_by_max_interval(ups.sort_by { |item| -item.y })
  end

  def antiideals
    threshold = [me.y, 0.25].min
    like_me_persons = like_me
    downs = persons.to_a.select { |item| item.y < threshold && !like_me_persons.include?(item) }
    downs.empty? ? [] : slice_persons_by_max_interval(downs.sort_by(&:y))
  end

  def toller_then_me
    i = ideals
    like_me_persons = like_me
    persons.to_a.select { |item| item.y > me.y && !i.include?(item) && !like_me_persons.include?(item)}
  end

  def worse_then_me
    anti = antiideals
    like_me_persons = like_me
    persons.to_a.select { |item| item.y < me.y && !anti.include?(item) && !like_me_persons.include?(item)}
  end

  def like_me
    persons.to_a.select { |item| (item.y - me.y).abs < 0.05 && item != me }
  end

  def soc_portrait_freqs
    res = {}
    res[VoluationProfile.ideals] = {'characteristics': frequencies_by_characteristics(ideals),
                                    'roles': frequencies_by_roles(ideals),
                                    'groups': frequencies_by_personal_values(ideals)}
    res[VoluationProfile.antiideals] = {'characteristics': frequencies_by_characteristics(antiideals),
                                        'roles': frequencies_by_roles(antiideals),
                                        'groups': frequencies_by_personal_values(antiideals)}
    res[VoluationProfile.toller_then_me] = {'characteristics': frequencies_by_characteristics(toller_then_me),
                                            'roles': frequencies_by_roles(toller_then_me),
                                            'groups': frequencies_by_personal_values(toller_then_me)}
    res[VoluationProfile.worse_then_me] = {'characteristics': frequencies_by_characteristics(worse_then_me),
                                           'roles': frequencies_by_roles(worse_then_me),
                                           'groups': frequencies_by_personal_values(worse_then_me)}
    res[VoluationProfile.like_me] = {'characteristics': frequencies_by_characteristics(like_me),
                                     'roles': frequencies_by_roles(like_me),
                                     'groups': frequencies_by_personal_values(like_me)}

    res
  end

  def self_esteem_type
    case me.y
    when 0.5..0.7
      SelfEsteemType.normal
    when 0.7..1
      SelfEsteemType.high
    else
      SelfEsteemType.reduced
    end
  end

  def validate
    i = me
    raise StandardError, 'Не определен персонаж Я' if i.blank?
    raise StandardError, 'Нет персонажей' if persons.size == 1

    persons.each do |p|
      next if p == i
      if p.person_roles.empty?
        raise StandardError, 'У персонажа ' + p.name + ' нет ролей'
      end
      if p.person_characteristics.empty?
        raise StandardError, 'У персонажа ' + p.name + ' нет характеристик'
      end
    end

  end

  def analyze
    validate
    fp = first_part
    sp = second_part
    tp = third_part

    ActiveRecord::Base.transaction do

      fp.save
      sp.save
      tp.save

      str = StudentTestResult.create(first_part_result: fp,
                                     second_part_result: sp,
                                     third_part_result: tp)

      update_attributes(student_test_result: str)
    end
  end

  private

  def first_part
    content = FirstPartResult.new
    n = persons.size - 1
    freqs = frequencies_by_roles(persons)
    freqs.each { |k, v| freqs[k] = 100 * v.to_f / n }
    max_freqs = max_frequences(freqs, 3)
    content.older_people = freqs['старше'].to_i
    content.peers = freqs['не старше'].to_i
    content.male = freqs['мужской пол'].to_i
    content.female = freqs['женский пол'].to_i
    content.relatives = freqs['родители'].to_i + freqs['родственники'].to_i
    content.real_comm = 100 - freqs['интернет-знакомые'].to_i - freqs['герои книг, фильмов'].to_i -
        freqs['герои компьютерных игр'].to_i - freqs['шоу-бизнес'].to_i -
        freqs['искусство, литература'].to_i - freqs['спорт'].to_i - freqs['наука'].to_i - freqs['политика'].to_i
    content.study_place = freqs['однокласcники/студенты'].to_i + freqs['работники школы(вуза)'].to_i
    content.inet_com = freqs['интернет-знакомые'].to_i + freqs['герои компьютерных игр'].to_i
    content.fict_chars = freqs['герои книг, фильмов'].to_i + freqs['герои компьютерных игр'].to_i +
        freqs['искусство, литература'].to_i
    content.famous = freqs['шоу-бизнес'].to_i + freqs['искусство, литература'].to_i + freqs['спорт'].to_i +
        freqs['наука'].to_i + freqs['политика'].to_i
    content.friends = content.real_comm - content.relatives
    content.study = freqs['работники школы(вуза)'].to_i +
        freqs['однокласники/студенты'].to_i + freqs['спорт'].to_i + freqs['наука'].to_i + freqs['политика'].to_i +
        freqs['искусство, литература'].to_i
    content.sport = freqs['спорт'].to_i
    content.science = freqs['наука'].to_i
    content.politics = freqs['политика'].to_i
    content.army = freqs['политика'].to_i
    content.art = freqs['искусство, литература'].to_i
    content.business = freqs['бизнес'].to_i
    content.max_categories = max_freqs.keys.join(', ')
    content.empty = (Set.new(Role.all.map(&:name)) - freqs.keys).to_a.join(', ')

    all_act_areas_freqs =
        freqs.select { |k, _| ['спорт', 'шоу-бизнес', 'наука', 'политика', 'искусство, литература'].include?(k) }
    max_act_areas_freqs = max_frequences(all_act_areas_freqs, 3)

    content.act_areas = max_act_areas_freqs.keys.join(', ')

    content
  end

  def second_part

    leading_values = {}
    leading_values_array = []

    ideals.each do |p|
      p.person_characteristics.each do |pc|
        group = pc.characteristic.personal_value_group.name
        leading_values[group] = Set.new unless leading_values.key?(group)
        leading_values_array << pc.characteristic
        leading_values[group] << pc.characteristic.name
      end
    end
    leading_values_line = []
    leading_values.each_pair { |k, v| leading_values_line << k.to_s + ': ' + v.to_a.join(', ') }

    #rejected = {}

    #leading_values_array.each do |ch|
    #  group = ch.personal_value_group.name
    #  rejected[group] = Set.new unless rejected.key?(group)
    #  rejected[group] << ch.first_chars.map(&:name)
    #  rejected[group] << ch.second_chars.map(&:name)

    #  rejected.delete(group) unless rejected[group].length == 0
    #end
    #rejected_line = []
    #rejected.each_pair { |k, v| rejected_line << k.to_s + ': ' + v.to_a.join(', ') }

    personal_resources = []
    toller_then_me.each do |p|
      personal_resources += p.person_characteristics.to_a.select { |item| item.characteristic.is_positive? }.map {|item| item.characteristic.name}
    end

    content = SecondPartResult.new
    content.leading_values = leading_values_line.join(', ')
    #content.rejected = rejected_line.join(', ')
    content.rejected = ''
    content.personal_resources = personal_resources.join(', ')

    content

  end

  def third_part
    content = ThirdPartResult.new
    ps_length = persons.size - 1
    content.optimist = case persons.to_a.select { |p| p.y > 0.5 && p != me }.length.to_f / ps_length
                       when 0.55..0.7
                         'В целом ты оптимист'
                       when 0.7..1
                         'Ты большой оптимист'
                       else
                         'Часто ты критически смотришь на все, что происходит'
                       end

    type = self_esteem_type
    content.self_esteem = if type.normal?
                            'Самооценка нормальная'
                          elsif type.high?
                            'Самооценка высокая'
                          else
                            'Самооценка снижена. Не стоит решать проблемы в одиночку. Объединись с теми, кому доверяешь. Смелее говори то, что думаешь.'
                          end

    content.equivalent = case persons.to_a.select { |p| (p.y - me.y).abs < 0.05 and p != me }.length.to_f / ps_length
                         when 0
                           'Ты чувствуешь свою уникальность среди других.'
                         when 1.0 / ps_length
                           'Ты чувствуешь свою индивидуальность, но некоторых воспринимаешь так же, как себя.'
                         when 0..0.25
                           'Ты чувствуешь индивидуальность себя и других, но часто воспринимаешь других наравне с собой.'
                         else
                           'Ты ценишь равенство людей и считаешь себя обычным человеком, таким как большинство других. Ты хочешь быть таким как все.'
                         end

    content.ideals = case ideals.length.to_f / ps_length
                     when 0.1..0.25
                       'Ты умеешь видеть лучшее в людях'
                     when 0.25..0.5
                       'При этом ты замечаешь самых лучших людей'
                     when 0.5..1
                       'И ты видишь в других большие достоинства и совершенство'
                     when 0
                       'Ты не видишь больших достоинств в других людях'
                     else
                       'При этом ты четко осознаешь свои идеалы'
                     end

    content.antiideals = case antiideals.length.to_f / ps_length
                         when 0
                           'Ты не веришь, что есть люди из одних недостатков. Ты веришь, что в каждом есть что-то хорошее'
                         when 0...0.1
                           'У тебя есть те, кого ты ненавидишь или презираешь. Ты не допускаешь в себе такие же качества, как у них. Ты борешься с этими качествами'
                         when 0.1..0.25
                           'В твоей жизни встречается много препятствий и проблем. Твои «антиидеалы»:' + 'Не стоит преодолевать их в одиночку. Надо объединиться с теми, кому ты доверяешь.' #TODO
                         else
                           'Тебе встречается много «плохих» людей. Твои «антиидеалы»: ' + 'Одному с этим трудно справиться.'
                         end
    content
  end

  def max_frequences(freqs, n)
    freqs.to_a.sort_by { |item| -item[1] }.slice(0, n).to_h
  end

  def slice_persons_by_max_interval(ps)
    intervals = []
    return(ps) if ps.size() == 1
    (0..ps.length - 2).each do |i|
      intervals << (ps[i].y - ps[i + 1].y).abs
    end
    max_el = intervals.max
    index = intervals.find_index(max_el)
    ps.slice(0, index + 1)
  end

  def me
    persons.to_a.select { |item| item.name == 'Я' }.first
  end

  def frequencies_by_roles(persons)
    freqs = {}

    persons.each do |p|
      p.person_roles.each do |r|
        key = r.role.name
        if !freqs.key?(key)
          freqs[key] = 1
        else
          freqs[key] += 1
        end
      end
    end

    freqs

  end

  def frequencies_by_personal_values(persons)
    freqs = {}

    persons.each do |p|
      p.person_characteristics.each do |c|
        key = c.characteristic.personal_value_group.name
        if !freqs.key?(key)
          freqs[key] = 1
        else
          freqs[key] += 1
        end
      end
    end

    freqs

  end

  def frequencies_by_characteristics(persons)
    freqs = {}

    persons.each do |p|
      p.person_characteristics.each do |c|
        key = c.characteristic.name
        if !freqs.key?(key)
          freqs[key] = 1
        else
          freqs[key] += 1
        end
      end
    end

    freqs

  end

end
