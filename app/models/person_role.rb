class PersonRole < ApplicationRecord
  belongs_to :person
  belongs_to :role

  validates_uniqueness_of :role, scope: :person

  filterrific :available_filters => [
      :with_person_id,
      :with_role_id
  ]

  scope :with_person_id, ->(id){
    where(:person_id => [*id])
  }

  scope :with_role_id, ->(id){
    where(:role_id => [*id])
  }
end
