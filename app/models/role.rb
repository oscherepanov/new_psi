class Role < ApplicationRecord
  validates :name, :role, presence: true
  validates :name, uniqueness: {case_sensitive: true}
  validates_uniqueness_of :role

  default_scope {order(position: :asc)}

  filterrific :available_filters => [
      :with_name
  ]

  scope :with_name, lambda { |query|
    return nil if query.blank?
    where("LOWER(name) LIKE ?", "%#{query}%".downcase)
  }


  def self.older
    find_by(name: 'старше')
  end

  def self.non_older
    find_by(name: 'не старше')
  end	

  def self.male
    find_by(name: 'мужской пол')
  end

  def self.female
    find_by(name: 'женский пол')
  end

  def self.par
    find_by(name: 'родители')
  end
  
  def self.relative
    find_by(name: 'родственники')
  end

  def self.classmates
    find_by(name: 'однокласники/студенты')
  end 

  def self.internet_friends
    find_by(name: 'интернет-знакомые')
  end

  def self.book_heroes 
    find_by(name: 'герои книг, фильмов')
  end 

  def self.comp_heroes
    find_by(name: 'герои компьютерных игр')
  end
  
  def self.kurgan_region 
    find_by(name: 'Курганская область')
  end

  def self.abroad
    find_by(name: 'Зарубежье')
  end
end
