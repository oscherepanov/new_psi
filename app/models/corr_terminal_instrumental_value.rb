class CorrTerminalInstrumentalValue < ApplicationRecord
  belongs_to :test_result
  belongs_to :voluation_profile

  validates :respondents_number, :without_respondents_number,
            numericality: {only_integer: true, greater_than_or_equal_to: 0}

  validates :frequency, numericality: {greater_than_or_equal_to: 0,
                                       less_than_or_equal_to: 1}

end
