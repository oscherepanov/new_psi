class VoluationProfile < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: {case_sensitive: true}

  def self.ideals
    find_by(name: 'идеалы')
  end

  def self.antiideals
    find_by(name: 'антиидеалы')
  end

  def self.toller_then_me
    find_by(name: 'лучше меня')
  end

  def self.worse_then_me
    find_by(name: 'хуже меня')
  end

  def self.like_me
    find_by(name: 'равноценные мне')
  end
end
