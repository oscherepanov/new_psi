class Antonym < ApplicationRecord
  belongs_to :first_char, class_name: "Characteristic"
  belongs_to :second_char, class_name: "Characteristic"

  filterrific available_filters: [
      :with_characteristic_id
  ]

  scope :with_characteristic_id, lambda { |id|
    where(first_char: [*id]).or(where(second_char: [*id]))
  }

end

