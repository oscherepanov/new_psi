class TestResult < ApplicationRecord
  has_many :hist_roles, dependent: :destroy
  has_many :hist_personal_value_groups, dependent: :destroy
  has_many :pair_roles, dependent: :destroy
  has_many :hist_role_personal_value_groups, dependent: :destroy
  has_many :authority_roles, dependent: :destroy
  has_many :soc_portraits, dependent: :destroy
  has_many :corr_terminal_instrumental_values, dependent: :destroy
  has_many :hist_self_esteems, dependent: :destroy
end
