class SecondPartResultSerializer < ActiveModel::Serializer
  attributes :leading_values, :rejected, :personal_resources
end
