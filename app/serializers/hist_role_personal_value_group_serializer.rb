class HistRolePersonalValueGroupSerializer < ActiveModel::Serializer
  attributes :role, :personal_value_group, :value

  def role
    self.object.role.name
  end

  def personal_value_group
    self.object.personal_value_group.name
  end
end
