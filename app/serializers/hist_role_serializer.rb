class HistRoleSerializer < ActiveModel::Serializer
  attributes :role_name, :generally, :indicate, :indicate_procent, :not_indicate, :not_indicate_procent
 
  def role_name
    self.object.role.name
  end
end
