class StudentSerializer < ActiveModel::Serializer
  attributes :id, :name, :sex, :student_group_id, :token
end
