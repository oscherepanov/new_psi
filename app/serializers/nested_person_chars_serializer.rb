class NestedPersonCharsSerializer < ActiveModel::Serializer
  attributes :id, :characteristic_id
end
