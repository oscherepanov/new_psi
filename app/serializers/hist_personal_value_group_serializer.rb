class HistPersonalValueGroupSerializer < ActiveModel::Serializer
  attributes :group_name, :group_human_name, :value, :indicate, :not_indicate, :indicate_percent,
             :not_indicate_percent, :value

  def group_name
    self.object.personal_value_group.name
  end

  def group_human_name
    self.object.personal_value_group.human_name
  end
end
