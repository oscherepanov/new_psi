class ThirdPartResultSerializer < ActiveModel::Serializer
  attributes :optimist, :self_esteem, :equivalent, :ideals, :antiideals
end
