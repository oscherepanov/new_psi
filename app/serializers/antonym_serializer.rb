class AntonymSerializer < ActiveModel::Serializer
  attributes :id, :first_char_id, :second_char_id
end
