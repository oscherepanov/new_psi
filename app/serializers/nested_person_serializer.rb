class NestedPersonSerializer < APersonSerializer
  has_many :person_roles, key: "person_roles_attributes",
           serializer: NestedPersonRoleSerializer
  has_many :person_characteristics, key: "person_characteristics_attributes",
           serializer: NestedPersonCharsSerializer
end
