class TestsController < ApplicationController
  include CrudUWD
  before_action :set_resource, only: [:show, :update, :destroy, :result]
  load_and_authorize_resource
  def index
    filterrific = initialize_filterrific(
        Test,
        params[:filterrific],
        sanitize_params: false,
        )
    tests = filterrific.find
    render json: tests.select {|item| can?(:show, item)}, status: :ok
  end

  def result
    begin
      @res.analyze unless @res.test_result
      render json: @res.test_result, status: :ok
    rescue StandardError => e
      render json: ApiError.new([e.message]), status: :conflict

    end
  end
  private

  def set_model
    @model = Test 
  end

  def resource_params
    params.permit(:student_group_id, :start, :finish)
  end
end
