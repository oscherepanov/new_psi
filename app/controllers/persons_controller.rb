class PersonsController < ApplicationController
  include CrudUWD
  load_and_authorize_resource

  def index
    filterrific = initialize_filterrific(
        Person,
        params[:filterrific],
        sanitize_params: false,
        )
    persons = filterrific.find
    render json: persons.select {|item| can?(:show, item)}, status: :ok
  end

  private

  def set_model
    @model = Person
  end

  def resource_params
    params.permit(:name, :x, :y, :student_test_id,
                  person_roles_attributes: %i[
                    id person_id role_id
                  ],
                  person_characteristics_attributes: %i[
                    id person_id characteristic_id
                  ])
  end

  def create_serializer
    PersonFullSerializer
  end
end
