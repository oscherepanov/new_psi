class CharacteristicsController < ApplicationController
  load_and_authorize_resource
  include CrudUWD
  def index
    filterrific = initialize_filterrific(
        Characteristic,
        params[:filterrific],
        sanitize_params: false,
    )
    render json: filterrific.find, status: :ok
  end

  private

  def set_model
    @model = Characteristic
  end

  def resource_params
    params.permit(:name, :positivity, :personal_value_group_id)
  end
end
