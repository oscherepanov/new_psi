class PersonRolesController < ApplicationController
  include CrudUWD
  load_and_authorize_resource

  def index
    filterrific = initialize_filterrific(
        PersonRole,
        params[:filterrific],
        sanitize_params: false,
        )
    person_roles = filterrific.find
    render json: person_roles.select {|item| can?(:show, item)}, status: :ok
  end

  private

  def set_model
    @model = PersonRole
  end

  def resource_params
    params.permit(:person_id, :role_id)
  end
end
