module Overrides
  class TokenValidationsController < DeviseTokenAuth::TokenValidationsController
    def validate_token
      if @resource
        render json: @resource
      else
        render :nothing => true, :status => 401
      end
    end
  end
end
