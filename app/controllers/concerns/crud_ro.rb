module CrudRO
  extend ActiveSupport::Concern
  included do
    before_action :set_model
    before_action :set_resource, only: [:show ]

    def index
      render json: @model.all, status: :ok 
    end

    def show
      render json: @res, status: :ok 
    end

    private
		    
    def set_resource
      @res = @model.find_by_id(params[:id])
      unless @res.present?
        record_not_found
      end
    end
  end
end
