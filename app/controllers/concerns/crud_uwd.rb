module CrudUWD
  extend ActiveSupport::Concern

  included do
   include CrudRO
   before_action :set_resource, only: [:show, :update, :destroy]
   def create
     resource = @model.new resource_params
     unless resource.valid?
       not_valid(resource.errors.full_messages)
       return
     end

     if resource.save
       render json: resource, status: :ok, serializer: create_serializer
     else
       can_not_save
     end
   end
   
   def update
     @res.attributes = resource_params
     unless @res.valid?
       not_valid @res.errors.full_messages
       return
     end
     authorize! :update, @res
     if @res.save
       render json: @res, status: :ok
     else
       can_not_save
     end
   end

   def destroy
     begin
       @res.destroy
       head :no_content
       rescue ActiveRecord::InvalidForeignKey => e
         render json: ApiError.new(['Удаление запрещено. На данный ресурс есть сторонние ссылки.']), status: :conflict 
       end
    end
  end

  private

  def create_serializer
    ActiveModel::Serializer.serializer_for(@model)
  end
end
