class AntonymPersonValueGroupsController < ApplicationController
  include CrudUWD
  load_and_authorize_resource

  def index
    filterrific = initialize_filterrific(
        AntonymPersonValueGroup,
        params[:filterrific],
        sanitize_params: false,
        )
    render json: filterrific.find, status: :ok
  end

  private

  def set_model
    @model = AntonymPersonValueGroup
  end

  def resource_params
    params.permit(:first_group_id, :second_group_id)
  end
end
