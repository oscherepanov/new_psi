class PersonalValueGroupsController < ApplicationController
  include CrudRO
  load_and_authorize_resource

  def index
    filterrific = initialize_filterrific(
        PersonalValueGroup,
        params[:filterrific],
        sanitize_params: false,
        )
    render json: filterrific.find, status: :ok
  end

  private

  def set_model
    @model = PersonalValueGroup 
  end
end
