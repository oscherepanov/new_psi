class CreatePairRole < ActiveRecord::Migration[5.2]
  def change
    create_table :pair_roles do |t|
      t.references :first_role, references: :role
      t.references :second_role, references: :role
      t.integer :eq_count
      t.integer :eq_percent
      t.integer :gt_count
      t.integer :gt_percent
      t.integer :lt_count
      t.integer :lt_percent
      t.timestamps
    end
  end
end
