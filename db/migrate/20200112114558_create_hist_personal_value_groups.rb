class CreateHistPersonalValueGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :hist_personal_value_groups do |t|
      t.references :test, foreign_key: true
      t.references :personal_value_group, foreign_key: true
      t.float :value
      t.integer :indicate
      t.float :indicate_percent
      t.float :not_indicate_percent
      t.integer :not_indicate

      t.timestamps
    end
  end
end
