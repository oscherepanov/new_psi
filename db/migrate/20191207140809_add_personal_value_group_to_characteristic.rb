class AddPersonalValueGroupToCharacteristic < ActiveRecord::Migration[5.2]
  def change
    add_reference :characteristics, :personal_value_group, index: true
  end
end
