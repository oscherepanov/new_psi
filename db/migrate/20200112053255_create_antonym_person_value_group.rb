class CreateAntonymPersonValueGroup < ActiveRecord::Migration[5.2]
  def change
    create_table :antonym_person_value_groups do |t|
      t.references :first_group, references: :personal_value_group
      t.references :second_group, references: :personal_value_group
      t.timestamps
    end
  end
end
