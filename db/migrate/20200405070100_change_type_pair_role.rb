class ChangeTypePairRole < ActiveRecord::Migration[5.2]
  def change
    change_column :pair_roles, :eq_percent, :float
    change_column :pair_roles, :lt_percent, :float
    change_column :pair_roles, :gt_percent, :float
  end
end
