class CreatePersonCharacteristics < ActiveRecord::Migration[5.2]
  def change
    create_table :person_characteristics do |t|
      t.references :person, foreign_key: true
      t.references :characteristic, foreign_key: true

      t.timestamps
    end
  end
end
