class CreateCorrTerminalInstrumentalValues < ActiveRecord::Migration[5.2]
  def change
    create_table :corr_terminal_instrumental_values do |t|
      t.references :test_result, foreign_key: true
      t.references :voluation_profile,foreign_key: true, index: {name: "index_ctiv_voluation_profile_id"}
      t.float :frequency
      t.integer :respondents_number
      t.integer :without_respondents_number

      t.timestamps
    end
  end
end
