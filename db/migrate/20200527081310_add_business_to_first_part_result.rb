class AddBusinessToFirstPartResult < ActiveRecord::Migration[5.2]
  def change
    add_column :first_part_results, :business, :integer
  end
end
