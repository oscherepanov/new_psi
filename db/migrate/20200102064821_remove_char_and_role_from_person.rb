class RemoveCharAndRoleFromPerson < ActiveRecord::Migration[5.2]
  def change
    remove_reference :people, :role, index: true, foreign_key: true
    remove_reference :people, :characteristic, index: true, foreign_key: true
  end
end
