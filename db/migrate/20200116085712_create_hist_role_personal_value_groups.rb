class CreateHistRolePersonalValueGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :hist_role_personal_value_groups do |t|
      t.references :role, foreign_key: true
      t.references :personal_value_group, foreign_key: true, index: {name: "index_hist_role_personal_value_group_id"} 
      t.references :test, foreign_key: true
      t.float :value

      t.timestamps
    end
  end
end
