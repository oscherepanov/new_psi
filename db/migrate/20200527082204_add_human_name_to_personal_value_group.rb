class AddHumanNameToPersonalValueGroup < ActiveRecord::Migration[5.2]
  def change
    add_column :personal_value_groups, :human_name, :string
  end
end
