class CreateThirdPartResults < ActiveRecord::Migration[5.2]
  def change
    create_table :third_part_results do |t|
      t.string :optimist
      t.string :self_esteem
      t.string :equivalent
      t.string :ideals
      t.string :antiideals

      t.timestamps
    end
  end
end
