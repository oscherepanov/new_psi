class AddSexToStudent < ActiveRecord::Migration[5.2]
  def change
    add_column :students, :sex, :integer
  end
end
