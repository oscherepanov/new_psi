class AddDirectionToHistRolePersonalValueGroup < ActiveRecord::Migration[5.2]
  def change
    add_column :hist_role_personal_value_groups, :direction, :integer 
  end
end
