FROM ruby:2.6.3
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client python-virtualenv

ENV TZ=Asia/Yekaterinburg
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY ./Gemfile* /psi/

WORKDIR /psi
RUN bundle install

COPY . /psi

CMD /psi/docker/prod-start.sh
